# Complete the calculate_average function which accepts
# a list of numerical values and returns the average of
# the numbers.
#
# If the list of values is empty, the function should
# return None
#
# Pseudocode is available for you

def calculate_average(values):
    total = 0
    if len(values) == 0:
        return None
    for num in values:
        total += num
    return (total/len(values))


# test case

# [25, 45, 65, 1, 100] = 47.2
values = [25, 45, 65, 1, 100]
print("expected: 47.2; actual: ",calculate_average(values))

#[10,15,16] = 13.6
values = [10,15,16]
print("expected: 13.6; actual: ",calculate_average(values))

#[] = None
values = []
print("expected: None; actual: ",calculate_average(values))
