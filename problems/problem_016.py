# Complete the is_inside_bounds function which takes an x
# coordinate and a y coordinate, and then tests each to
# make sure they're between 0 and 10, inclusive.

def is_inside_bounds(x, y):
    if x >= 0 and y >= 0 and x <= 10 and y <= 10:
        return True
    return False

#test case

# 6,8 = true
x = 6
y = 8
print("expected: True; Actual: ", is_inside_bounds(x,y))
# 0,10 = true
x = 0
y = 10
print("expected: True; Actual: ", is_inside_bounds(x,y))
# 7,13 = false
x = 7
y = 13
print("expected: False; Actual: ", is_inside_bounds(x,y))
# -1,11 = false
x = -1
y = 11
print("expected: False; Actual: ", is_inside_bounds(x,y))
