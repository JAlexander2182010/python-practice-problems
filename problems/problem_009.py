# Complete the is_palindrome function to check if the value in
# the word parameter is the same backward and forward.
#
# For example, the word "racecar" is a palindrome because, if
# you write it backwards, it's the same word.

# It uses the built-in function reversed and the join method
# for string objects.

# Do some planning in ./planning.md

# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.

def is_palindrome(word):
    word_without_spaces = word.replace(" ", "")
    reversed_word = word_without_spaces[::-1]

    if word_without_spaces == reversed_word:
        return "This IS a palindrome"
    else:
        return "This is NOT a palindrome"


#test cases

#racecar = is pali
word = "racecar"
print("Expected: ",word," This IS a palindrome; Actual result:", is_palindrome(word))

#level = is pali
word = "level"
print("Expected: ",word," This IS a palindrome; Actual result:", is_palindrome(word))

#spaceship = not pali
word = "spaceship"
print("Expected: ",word," This is NOT a palindrome; Actual result:", is_palindrome(word))

#taco cat = is pali
word = "taco cat"
print("Expected: ",word," This IS a palindrome; Actual result:", is_palindrome(word))

#my gym = is pali
word = "my gym"
print("Expected: ",word," This IS a palindrome; Actual result:", is_palindrome(word))

#space man = not pali
word = "space man"
print("Expected: ",word," This is NOT a palindrome; Actual result:", is_palindrome(word))
