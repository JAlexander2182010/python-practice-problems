# Complete the gear_for_day function which returns a list of
# gear needed for the day given certain criteria.
#   * If the day is not sunny and it is a workday, the list
#     needs to contain "umbrella"
#   * If it is a workday, the list needs to contain "laptop"
#   * If it is not a workday, the list needs to contain
#     "surfboard"

def gear_for_day(is_workday, is_sunny):
    list_of_needed_items = []
    umbrella = "umbrella"
    surfboard = "surfboard"
    laptop = "laptop"

    if is_workday:
        list_of_needed_items.append(laptop)
        if not is_sunny:
            list_of_needed_items.append(umbrella)
            return list_of_needed_items
        return list_of_needed_items
    elif not is_workday and is_sunny:
        list_of_needed_items.append(surfboard)
        return list_of_needed_items
    else:
        return "Guess your not going out today"


#test cases

#sunny and not workday = surfboard
is_workday = False
is_sunny = True
print("expected: surfboard; actual: ", gear_for_day(is_workday, is_sunny))

#not sunny and is workday = laptop, umbrella
is_workday = True
is_sunny = False
print("expected: laptop, umbrella: ", gear_for_day(is_workday, is_sunny))

#not sunny and not workday = not going out
is_workday = False
is_sunny = False
print("expected: not going out: ", gear_for_day(is_workday, is_sunny))

#sunny and is workday = laptop
is_workday = True
is_sunny = True
print("expected: laptop: ", gear_for_day(is_workday, is_sunny))
