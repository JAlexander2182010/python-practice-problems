# Complete the calculate_grade function which accepts
# a list of numerical scores each between 0 and 100.
#
# Based on the average of the scores, the function
# returns
#   * An "A" for an average greater than or equal to 90
#   * A "B" for an average greater than or equal to 80
#     and less than 90
#   * A "C" for an average greater than or equal to 70
#     and less than 80
#   * A "D" for an average greater than or equal to 60
#     and less than 70
#   * An "F" for any other average

def calculate_grade(values):
    total = 0
    for score in values:
        total += score

    average = total / len(values)

    if average >= 90:
        return "Grade: A"
    elif average >= 80 and average < 90:
        return "Grade: B"
    elif average >= 70 and average < 80:
        return "Grade: C"
    elif average >= 60 and average < 70:
        return "Grade: D"
    else:
        return "Grade: F"


#test cases

#[80,60,90,65] = C
values = [80,60,90,65]
print("expected: C; actual: ", calculate_grade(values))
#[100,100,90,87] = A
values = [100,100,90,87]
print("expected: A; actual: ", calculate_grade(values))
#[10,30,50,0] = F
values = [10,30,50,0]
print("expected: F; actual: ", calculate_grade(values))
