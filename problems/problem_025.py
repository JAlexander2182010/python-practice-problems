# Complete the calculate_sum function which accepts
# a list of numerical values and returns the sum of
# the numbers.
#
# If the list of values is empty, the function should
# return None
#

def calculate_sum(values):
    sum = 0
    if len(values) == 0:
        return None
    for value in values:
        sum += value
    return sum

#test case

#[1,2,3,4,5] expect 15
list = [1,2,3,4,5]
print("expected: 15; actual: ", calculate_sum(list))

#[] expect None
list = []
print("expected: None; actual: ", calculate_sum(list))

#[1] expect 1
list = [1]
print("expected: 1; actual: ", calculate_sum(list))

#[0,0,0,0] expect 0
list = [0,0,0,0]
print("expected: 0; actual: ", calculate_sum(list))
