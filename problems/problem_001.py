# Complete the minimum_value function so that returns the
# minimum of two values.
#
# If the values are the same, return either.

# Do some planning in ./planning.md

# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.

def minimum_value(value1, value2):
    if value1 < value2:
        return value1
    return value2

#test cases
#expected 14
print("The expected is 14, the result is: ", minimum_value(14, 18))
#expected 2
print("The expected is 2, the result is: ", minimum_value(8, 2))
#expected 11
print("The expected is 11, the result is: ", minimum_value(11, 11))
