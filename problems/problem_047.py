# Complete the check_password function that accepts a
# single parameter, the password to check.
#
# A password is valid if it meets all of these criteria
#   * It must have at least one lowercase letter (a-z)
#   * It must have at least one uppercase letter (A-Z)
#   * It must have at least one digit (0-9)
#   * It must have at least one special character $, !, or @
#   * It must have six or more characters in it
#   * It must have twelve or fewer characters in it
#
# The string object has some methods that you may want to use,
# like ".isalpha", ".isdigit", ".isupper", and ".islower"

def check_password(password):
    list_of_checks = [["lowercase",False], ["uppercase", False], ["digit", False], ["special Char", False]]
    if len(password) >= 6 and len(password) <= 12:
        for char in password:
            if char.isalpha() and char.islower:
                list_of_checks[0][1] = True
            if char.isalpha() and char.isupper:
                list_of_checks[1][1] = True
            if char.isdigit:
                list_of_checks[2][1] = True
            if char == "$" or char == "!" or char == "!":
                list_of_checks[3][1] = True
        for index in range(len(list_of_checks)):
            if list_of_checks[index][1] == False:
                return "password does not meet the criteria"
        return "Your password is accepted!!!!"
    else:
        return "Password length must be 6 - 12 characters long"


#test case

#expected: accepted
password = "Hell0!"
print(check_password(password))

#expected: length too long
password = "Hell0!1111111111"
print(check_password(password))

#expected: missing criteria
password = "Hello111"
print(check_password(password))
