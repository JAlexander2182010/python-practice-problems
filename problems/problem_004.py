# Complete the max_of_three function so that returns the
# maximum of three values.
#
# If two values are the same maximum value, return either of
# them.
# If the all of the values are the same, return any of them
#
# Use the >= operator for greater than or equal to

# Do some planning in ./planning.md

# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.

def max_of_three(value1, value2, value3):
    list_of_values = [value1, value2, value3]
    max_value = value1

    for value in list_of_values:
        if value > max_value:
            max_value = value
    return max_value

#test cases
#expected value 9
print("The expected value is 9, actual value is: ", max_of_three(3,6,9))
#expected value 20
print("The expected value is 20, actual value is: ", max_of_three(20,13,18))
#expected value 12
print("The expected value is 12, actual value is: ", max_of_three(11,11,12))
#expected value 72
print("The expected value is 72, actual value is: ", max_of_three(72,72,1))
