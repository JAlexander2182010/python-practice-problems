# Complete the sum_of_first_n_even_numbers function which
# accepts a numerical count n and returns the sum of the
# first n even numbers
#
# If the value of the limit is less than 0, then it should
# return None
#
# Examples:
#   * -1 returns None
#   * 0 returns 0
#   * 1 returns 0+2=2
#   * 2 returns 0+2+4=6
#   * 5 returns 0+2+4+6+8+10=30
#
# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.

def sum_of_first_n_even_numbers(n):
    if n <= 0:
        return None
    counter = 0
    sum = 0
    num = 0
    while counter <= n:
        if num % 2 == 0:
            sum += num
            counter += 1
        num += 1
    return sum

#test case

# test 0 expected None
num = 0
print(sum_of_first_n_even_numbers(num))
#test negative expected None
num = -1
print(sum_of_first_n_even_numbers(num))
#test 1 expected 2
num = 1
print(sum_of_first_n_even_numbers(num))
#test 2 expected 6
num = 2
print(sum_of_first_n_even_numbers(num))
#test 5 expected 30
num = 5
print(sum_of_first_n_even_numbers(num))
