# Complete the max_in_list function to find the
# maximum value in a list
#
# If the list is empty, then return None.
#

def max_in_list(values):
    if len(values) == 0:
        return None

    max_value = values[0]

    for value in values:
        if value > max_value:
            max_value = value
    return max_value



#test case

#[] = None
values = []
print("expected: None; actual: ",max_in_list(values))

#[1,2,3,4,5,6] = 6
values = [1,2,3,4,5,6]
print("expected: 6; actual: ",max_in_list(values))

#[23,5] = 23
values = [23,5]
print("expected: 23; actual: ",max_in_list(values))

#[100,55,900,50] = 900
values = [100,55,900,50]
print("expected: 900; actual: ",max_in_list(values))
