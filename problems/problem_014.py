# Complete the can_make_pasta function to
# * Return true if the ingredients list contains
#   "flour", "eggs", and "oil"
# * Otherwise, return false
#
# The ingredients list will always contain three items.

# Do some planning in ./planning.md

# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.

def can_make_pasta(ingredients):
    required_ingredients = ["eggs", "flour", "oil"]
    checklist = 0
    for ingredient in ingredients:
        for check_list in required_ingredients:
            if check_list == ingredient:
                checklist += 1
    if checklist == 3:
        return True
    return False


#test case

#egg, oil, fish = false
onhand_ingredients = ["eggs", "oil", "fish"]
print("Expected: False; Actual: ", can_make_pasta(onhand_ingredients))
#egg, oil, flour = true
onhand_ingredients = ["eggs", "oil", "flour"]
print("Expected: True; Actual: ", can_make_pasta(onhand_ingredients))
#flour, eggs, oil = True
onhand_ingredients = ["flour", "eggs", "oil"]
print("Expected: True; Actual: ", can_make_pasta(onhand_ingredients))
#bacon, bacon, bacon = false
onhand_ingredients = ["bacon", "bacon", "bacon"]
print("Expected: False; Actual: ", can_make_pasta(onhand_ingredients))
