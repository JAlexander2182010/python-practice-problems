# Complete the has_quorum function to test to see if the number
# of the people in the attendees list is greater than or equal
# to 50% of the number of people in the members list.

def has_quorum(attendees_list, members_list):
    members_quota = members_list / 2

    if attendees_list >= members_quota:
        return "50% or more attended"
    return "less than 50% attended"

#test case

#attendees = 75, members = 100; expect: "50% or more attended"
attendees_list = 75
members_list = 100
print("Expected:'50% or more attended'; actual: ", has_quorum(attendees_list,members_list))

#attendees = 25, members = 75; expect: "less than 50% attended"
attendees_list = 25
members_list = 75
print("Expected:'less than 50% attended'; actual: ", has_quorum(attendees_list,members_list))
