# Write a function that meets these requirements.
#
# Name:       safe_divide
# Parameters: two values, a numerator and a denominator
# Returns:    if the denominator is zero, then returns math.inf.
#             otherwise, returns numerator / denominator
#
# Don't for get to import math!
import math


def safe_divide(numerator, denominator):
    if denominator == 0:
        return math.inf
    return numerator/denominator

#test cases

# 50/2 = 25
numerator = 50
denominator = 2
print(safe_divide(numerator, denominator))

#60/0 = math.inf
numerator = 60
denominator = 0
print(safe_divide(numerator, denominator))
