# Complete the add_csv_lines function which accepts a list
# as its only parameter. Each item in the list is a
# comma-separated string of numbers. The function should
# return a new list with each entry being the corresponding
# sum of the numbers in the comma-separated string.
#
# These kinds of strings are called CSV strings, or comma-
# sepearted values strings.
#
# Examples:
#   * input:  []
#     output: []
#   * input:  ["3", "1,9"]
#     output: [3, 10]
#   * input:  ["8,1,7", "10,10,10", "1,2,3"]
#     output:  [16, 30, 6]
#
# Look up the string split function to find out how to
# split a string into pieces.

# Write out your own pseudocode to help guide you.

def add_csv_lines(csv_lines):
    list_added = []
    for index in csv_lines:
        index_sum = 0
        index_split = index.split(",")
        for num in index_split:
            index_sum += int(num)
        list_added.append(index_sum)
    return list_added


#test cases

#expected [] = []
list = []
print(add_csv_lines(list))

#expected [3, 10] = [3, 10]
list = ["3", "1,9"]
print(add_csv_lines(list))

#expected ["8,1,7", "10,10,10", "1,2,3"] = [16, 30, 6]
list = ["8,1,7", "10,10,10", "1,2,3"]
print(add_csv_lines(list))
