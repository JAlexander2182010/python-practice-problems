# Complete the can_skydive function so that determines if
# someone can go skydiving based on these criteria
#
# * The person must be greater than or equal to 18 years old, or
# * The person must have a signed consent form

# Do some planning in ./planning.md

# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.

def can_skydive(age, has_consent_form):
    if age < 18 and has_consent_form or age >= 18:
        return "Skydiving: Access Granted"
    else:
        return "Skydiving: Access Denied"

# Test cases

#Age 16, consent = true, expected: granted
age = 16
consent_form = True
print("Expected result: granted; actual result: ", can_skydive(age, consent_form))

#Age 16, consent = true, expected: granted
age = 18
consent_form = False
print("Expected result: granted; actual result: ", can_skydive(age, consent_form))

#Age 16, consent = true, expected: denied
age = 15
consent_form = False
print("Expected result: denied; actual result: ", can_skydive(age, consent_form))
