# Complete the reverse_dictionary function which has a
# single parameter that is a dictionary. Return a new
# dictionary that has the original dictionary's values
# for its keys, and the original dictionary's keys for
# its values.
#
# Examples:
#   * input:  {}
#     output: {}
#   * input:  {"key": "value"}
#     output: {"value", "key"}
#   * input:  {"one": 1, "two": 2, "three": 3}
#     output: {1: "one", 2: "two", 3: "three"}

def reverse_dictionary(dictionary):
    new_dict = {}
    for key in dict_test:
        new_dict[dictionary[key]] = key
    return new_dict


#test case

#expected result {1: "one", 2: "two", 3: "three"}
dict_test = {"one": 1, "two": 2, "three": 3}
print(reverse_dictionary(dict_test))

#{"value", "key"}
dict_test = {"key": "value"}
print(reverse_dictionary(dict_test))

#{}
dict_test = {}
print(reverse_dictionary(dict_test))
